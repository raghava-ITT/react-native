import React from 'react';
import { StyleSheet, Text, View, TouchableNativeFeedbackComponent } from 'react-native';
import Header from '../Header/Header';
import TodoComponent from '../Todo/Todo'

class MainLayout extends React.Component {
    state = {
        taskList : [],
        CurrentTask : "",
        isAdd : false
    }
    
    componentDidMount(){
        let stringifyList = localStorage.getItem('ListOfItem');
        if(stringifyList && this.state.taskList.length == 0){
            let parsedList = JSON.parse(stringifyList);
            this.setState({
                taskList:parsedList
            })
        }
    }

    onAdd= ()=>{
     this.setState({
        isAdd : true
    });
    }

    clearAll = () => {
        this.setState({
            taskList:[]
        })
        localStorage.setItem('ListOfItem',[]);
    }

    toggleEdit = (index) => {
        let taskList = [...this.state.taskList];
        taskList[index].isEdit = !taskList[index].isEdit;
        localStorage.setItem('ListOfItem',JSON.stringify(taskList))
        this.setState({
            taskList : taskList
        })
    }
    
    updateTaskNameByIndex = (index,name) => {
        let taskList = [...this.state.taskList];
        taskList[index].name = name;
        localStorage.setItem('ListOfItem',JSON.stringify(taskList))
        this.setState({
            taskList : taskList
        })
    }
    
    onChange = (index) => {
        let taskList = [...this.state.taskList];
        taskList[index].isDone = !taskList[index].isDone;
        localStorage.setItem('ListOfItem',JSON.stringify(taskList))
        this.setState({
            taskList : taskList
        })
    }

    updateTaskName = (name) => {
        this.setState({
            CurrentTask : name
        })
    }

    deleteTask = (index) => {
        let taskList =this.state.taskList.splice(index,1);
        localStorage.setItem('ListOfItem',JSON.stringify(taskList))
        this.setState({
            taskList: taskList
        })
    }

    addTask = () => {
        if(!this.state.CurrentTask)
            return;
        let taskList =  [...this.state.taskList];
        taskList.push({
            name : this.state.CurrentTask,
            isDone : false,
            isEdit : false
        });
        localStorage.setItem('ListOfItem',JSON.stringify(taskList))
        this.setState({
            taskList:taskList,
            CurrentTask:"",
            isAdd : false
        })
    }

    render(){
        return (
            <View style = {styles.container}>
                <Header title="My To Do List" clearAll={this.clearAll } onAdd = {this.onAdd} />
                <TodoComponent 
                    taskList={this.state.taskList} 
                    currentTask={this.state.CurrentTask}
                    onChange={this.onChange}
                    updateTaskName = {this.updateTaskName}
                    deleteTask = {this.deleteTask}
                    addTask = {this.addTask}
                    isAdd = {this.state.isAdd}
                    toggleEdit = {this.toggleEdit}
                    updateTaskNameByIndex = {this.updateTaskNameByIndex}
                    >

                </TodoComponent>
            </View>
        )
    }   
}

export default MainLayout;
const styles = StyleSheet.create({
    container: { 
        margin : 100 ,
      borderWidth :5,
      borderColor : "green",
    },
});