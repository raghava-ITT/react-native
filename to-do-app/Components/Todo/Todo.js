import React from 'react';
import { render } from 'react-dom';
import { StyleSheet, Text, View, TextInput, CheckBox, Button, TouchableOpacity } from 'react-native';

const Todolist = (props) => {
    return (
       
        <View style={styles.container}>
          {props.isAdd ?  <View  style={{flexDirection:"row",marginTop:25, height : 50}}>
            <TextInput
                value={props.currentTask}
                style={{width:"50%", marginRight:15, backgroundColor: "#F5F5F5"}}
                placeholder="  Enter here"
                onChangeText={text => props.updateTaskName(text)}
                onBlur = {props.addTask}
            />
        </View> : <View></View>}
            
            {props.taskList.map((task,i) => {
                return (
                    <View key={i}>
                        {task.isEdit ? 
                            <View style={{flexDirection:"row",marginTop:10,marginBottom:10}}>
                                <TextInput
                                    value={task.name}
                                    style={{width:"30%", marginRight:15,  backgroundColor: "#F5F5F5"}}
                                    placeholder="Enter task name"
                                    onChangeText={text => props.updateTaskNameByIndex(i,text)}
                                /> 
                               
                                <View style={styles.button}>
                                    <Button title="Save" color = "steelblue" onPress={() => props.toggleEdit(i)}/>
                                </View> 
                            </View>: 
                            <View style={{flexDirection:"row",marginTop:10,marginBottom:10}}>
                                 <CheckBox
                                    value={task.isDone}
                                    onChange={(val) => props.onChange(i)}
                                    style={styles.checkbox}
                                />
                                <Text style= {task.isDone ? {width:"40%",alignSelf : "center", paddingLeft : 20, textDecorationLine: 'line-through', textDecorationStyle: 'solid'} : {width:"40%",alignSelf : "center", paddingLeft : 20}}><b>{task.name}</b></Text>
                                <View style={styles.button}>
                                    <Button title="Delete" color = "steelblue" onPress={() => props.deleteTask(i)}/>
                                </View> 
                                {!task.isDone ?
                                <View style={styles.button}>
                                    <Button title="Edit" color = "steelblue" onPress={() => props.toggleEdit(i)}/>
                                </View> : <View></View>
                                }
                            </View>
                        }    
                        
                    </View>
                )
            })}
        </View>
    )
}

export default Todolist;

const styles = StyleSheet.create({
    checkbox: {
        alignSelf: "center",
    },
    container: {
        marginTop:10+'%',
        paddingLeft:25,
        marginLeft: 250
      },
    button: {
        position:"relative",
        left:"7%",
        marginLeft:10
    }
});