import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

const Header = (props) => {
    return (
        <View style={styles.container}>
            <View style={{width:400}}>
    <           Text style={styles.headerText}><b>{props.title}</b></Text>
            </View>
            <View style={{padding:10 , justifyContent : "center"}} ><Button color = "steelblue" title="Add ToDo" onPress={props.onAdd} /></View>
            <View style={{padding:10, justifyContent : "center"}} ><Button color = "steelblue" title="ClearAll" onPress={props.clearAll} /></View>
        </View>
        
    );
}

export default Header;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",
        backgroundColor: 'whitesmoke',
        textAlign : 'center',
        justifyContent : 'center',
        position: 'absolute',
        marginTop: 30,
        marginLeft: 250,
        height: 60,
        width: 650
    },
    headerText: {
        fontSize : 24,
        width: "70%",
        padding:7
    }
});